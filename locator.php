<?php

require __DIR__ . '/vendor/autoload.php';

use Ups\Locator\Locator;
use Ups\Locator\LocatorCountryCodes;
use Ups\Locator\LocatorUnitsOfMeasurement;

session_start();

$measurements = LocatorUnitsOfMeasurement::getConstants();
$codes = LocatorCountryCodes::getConstants();

$locations = [];

if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_SESSION['token'])&& ($_POST['token'] == $_SESSION['token']))
{
    $locator = new Locator("AccessLicenseNumber", "UserId", "Password");
    $locator->setPoliticalDivision2($_POST['PoliticalDivision2']);
    $locator->setCountryCode($_POST['CountryCode']);
    $locator->setUnitOfMeasurement($_POST['Code']);
    $locator->setMaximumListSize(3);
    $locator->setSearchRadius($_POST['ServiceSearch']);
    $response = $locator->run(function($http_code, $results)
    {
        if ($http_code != 200)
        {
            throw new Exception("API error (http code: {$http_code}).", 404);
        }

        return json_decode($results);
    });

    if (@$response->LocatorResponse->Response->ResponseStatusCode == '1')
    {
        if ($results = @$response->LocatorResponse->SearchResults->DropLocation)
        {
            $locations = $results;
        }
    }
}

$token = md5(uniqid(mt_rand()));
$_SESSION['token'] = $token;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <meta name="skype_toolbar" content="skype_toolbar_parser_compatible">
    <title>UPS Locator</title>
    <style>

        label {
            display: block;
            margin-bottom: 20px;
        }
        label > * {
            display: block;
            margin-top: 4px;
        }

        .location {
            border-top: 1px dotted rgba(0,0,0,0.5);
            border-bottom: 1px dotted rgba(0,0,0,0.5);
            padding-top: 20px;
            padding-bottom: 20px;
        }

        .location + .location {
            border-top: none;
            padding-top: 0;
        }
        * + .location {
            margin-top: 20px;
        }
        .location > * {
            display: block;
        }

    </style>
</head>
<body>


<form action="" method="post">

    <input type="hidden" name="token" value="<?=$token;?>">

    <label>
        City or Town
        <input type="text" name="PoliticalDivision2">
    </label>

    <label>
        State or province
        <input type="text" name="PoliticalDivision1">
    </label>

    <label>
        Postcode
        <input type="text" name="PostcodePrimaryLow">
    </label>

    <label>
        Search Radius
        <input type="text" name="ServiceSearch">
    </label>

    <label>
        Units of Measurement
        <select name="Code">
            <?php foreach ($measurements as $k => $v): ?>

                <option value="<?=$k;?>"><?=$v;?></option>

            <?php endforeach; ?>
        </select>
    </label>

    <label>
        Select a Country
        <select name="CountryCode">
            <?php foreach ($codes as $k => $v): ?>

                <option value="<?=$k;?>"><?=$v;?></option>

            <?php endforeach; ?>
        </select>
    </label>

    <button type="submit">Submit</button>

</form>

<?php if ($locations): ?>
    <?php foreach ($locations as $location): ?>

        <address class="location">


            <span class="location__id"><?=$location->LocationID;?></span>

            <span class="location__name"><?=$location->AddressKeyFormat->ConsigneeName;?></span>

            <span class="location__address"><?=$location->AddressKeyFormat->AddressLine;?></span>

            <span class="location__town"><?=$location->AddressKeyFormat->PoliticalDivision2;?></span>

            <span class="location__postcode"><?=$location->AddressKeyFormat->PostcodePrimaryLow;?></span>

            <span class="location__country"><?=$location->AddressKeyFormat->CountryCode;?></span>

            <span class="location__distance"><?=$location->Distance->Value;?> <?=$location->Distance->UnitOfMeasurement->Description;?></span>

            <?php if ($SpecialInstructions = @$location->SpecialInstructions->Segment): ?>
                <span class="location__extra"><?=$SpecialInstructions;?></span>
            <?php endif; ?>

            <span class="location__description"><?=$location->LocationAttribute->OptionCode->Description;?></span>

            <span class="location__hours"><?=$location->StandardHoursOfOperation;?></span>

        </address>

    <?php endforeach; ?>
<?php endif; ?>

</body>
</html>