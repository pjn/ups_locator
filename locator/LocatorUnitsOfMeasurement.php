<?php namespace Ups\Locator;



class LocatorUnitsOfMeasurement {

    use ConstantTrait;

    const MILES = "MI";
    const KILOMETERS = "KM";

}