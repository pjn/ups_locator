<?php namespace Ups\Locator;


use Exception;


class Locator {

    const UPS_LIVE_URL = "https://onlinetools.ups.com/rest/Locator";
    const UPS_DEV_URL = "https://wwwcie.ups.com/rest/Locator";

    private $AccessLicenseNumber;
    private $UserId;
    private $Password;
    private $maximumListSize = 2;
    private $searchRadius = 2;
    private $countryCode;
    private $AddressLine;
    private $PoliticalDivision2;
    private $PoliticalDivision1;
    private $PostcodePrimaryLow;
    private $PostcodeExtendedLow;
    private $UnitOfMeasurement;


    public function __construct($AccessLicenseNumber, $UserId, $Password)
    {
        $this->setAccessLicenseNumber($AccessLicenseNumber);
        $this->setUserId($UserId);
        $this->setPassword($Password);
    }


    /**
     * @return string
     */
    private function getAccessLicenseNumber()
    {
        return $this->AccessLicenseNumber;
    }

    /**
     * @param string $AccessLicenseNumber
     */
    private function setAccessLicenseNumber($AccessLicenseNumber)
    {
        $this->AccessLicenseNumber = $AccessLicenseNumber;
    }

    /**
     * @return string
     */
    private function getUserId()
    {
        return $this->UserId;
    }

    /**
     * @param string $UserId
     */
    private function setUserId($UserId)
    {
        $this->UserId = $UserId;
    }

    /**
     * @return string
     */
    private function getPassword()
    {
        return $this->Password;
    }

    /**
     * @param string $Password
     */
    private function setPassword($Password)
    {
        $this->Password = $Password;
    }

        /**
     * @return string
     */
    public function getUnitOfMeasurement()
    {
        return $this->UnitOfMeasurement;
    }

    /**
     * @param string $UnitOfMeasurement
     */
    public function setUnitOfMeasurement($UnitOfMeasurement)
    {
        $this->UnitOfMeasurement = $UnitOfMeasurement;
    }


    /**
     * @return int
     */
    public function getMaximumListSize()
    {
        return $this->maximumListSize;
    }

    /**
     * @param int $maximumListSize
     */
    public function setMaximumListSize($maximumListSize)
    {
        $this->maximumListSize = $maximumListSize;
    }

    /**
     * @return int
     */
    public function getSearchRadius()
    {
        return $this->searchRadius;
    }

    /**
     * @param int $searchRadius
     */
    public function setSearchRadius($searchRadius)
    {
        $this->searchRadius = $searchRadius;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return string
     */
    public function getAddressLine()
    {
        return $this->AddressLine;
    }

    /**
     * @param string $AddressLine
     */
    public function setAddressLine($AddressLine)
    {
        $this->AddressLine = $AddressLine;
    }

    /**
     * @return string
     */
    public function getPoliticalDivision2()
    {
        return $this->PoliticalDivision2;
    }

    /**
     * @param string $PoliticalDivision2
     */
    public function setPoliticalDivision2($PoliticalDivision2)
    {
        $this->PoliticalDivision2 = $PoliticalDivision2;
    }

    /**
     * @return string
     */
    public function getPoliticalDivision1()
    {
        return $this->PoliticalDivision1;
    }

    /**
     * @param string $PoliticalDivision1
     */
    public function setPoliticalDivision1($PoliticalDivision1)
    {
        $this->PoliticalDivision1 = $PoliticalDivision1;
    }

    /**
     * @return mixed
     */
    public function getPostcodePrimaryLow()
    {
        return $this->PostcodePrimaryLow;
    }

    /**
     * @param mixed $PostcodePrimaryLow
     */
    public function setPostcodePrimaryLow($PostcodePrimaryLow)
    {
        $this->PostcodePrimaryLow = $PostcodePrimaryLow;
    }

    /**
     * @return mixed
     */
    public function getPostcodeExtendedLow()
    {
        return $this->PostcodeExtendedLow;
    }

    /**
     * @param mixed $PostcodeExtendedLow
     */
    public function setPostcodeExtendedLow($PostcodeExtendedLow)
    {
        $this->PostcodeExtendedLow = $PostcodeExtendedLow;
    }


    /**
     * Posts a json object to get results form UPS Locator API.
     * @param callable|null $callback
     * @return array|mixed
     * @throws Exception
     */
    public function run(callable $callback=null)
    {
        $data = [
            "AccessRequest" => [
                "AccessLicenseNumber" => $this->getAccessLicenseNumber(),
                "UserId" => $this->getUserId(),
                "Password" => $this->getPassword(),
            ],
            "LocatorRequest" => [
                "Request" => [
                    "RequestAction" => "Locator",
                    //Types of Requests
                    //1 A list of locations
                    //8 All available additional services
                    //16 All available program types
                    //24 All available additional services and program types
                    //32 All available retail locations
                    //40 All available retail locations and additional services
                    //48 All available retail locations and program types
                    //56 All available retail locations, program types, and additional services
                    //64 UPS Access Point Search
                    "RequestOption" => "64",
                    "TransactionReference" => [
                        "CustomerContext" => "",
                    ],
                ],
                "OriginAddress" => [
                    "AddressKeyFormat" => [
                        "AddressLine" => $this->getAddressLine(),
                        "PoliticalDivision2" => $this->getPoliticalDivision2(),
                        "PoliticalDivision1" => $this->getPoliticalDivision1(),
                        "PostcodePrimaryLow" => $this->getPostcodePrimaryLow(),
                        "PostcodeExtendedLow" => $this->getPostcodeExtendedLow(),
                        "CountryCode" => $this->getCountryCode(),
                    ]
                ],
                "Translate" => "",
                "UnitOfMeasurement" => [
                    "Code" => $this->getUnitOfMeasurement(),
                ],
                "LocationSearchCriteria"  => [
                    "SearchOption" => [
                        "OptionType" => [
                            //01-Location
                            //02-Retail Location
                            //03-Additional Services
                            //04-Program Type
                            "Code" => "01",
                        ],
                        "OptionCode" => [
                            //Location Options:
                            //000 Represents all location types supported by a country (not an actual location type code)
                            //001 UPS Customer Center
                            //002 The UPS Store
                            //003 UPS Drop Box
                            //004 Authorized Shipping Outlet
                            //005 Mail Boxes Etc.
                            //    007 UPS Alliance
                            //009 UPS Express
                            //010 UPS Express
                            //011 UPS Express
                            //012 UPS Express
                            //014 UPS Authorized Service Providers
                            //015 UPS Authorized Service Providers
                            //016 UPS Authorized Service Providers
                            //017 UPS Worldwide Express Freight Center
                            //018 UPS Access Point
                            //019 UPS Access Point Locker
                            "Code" => "018",
                        ]
                    ],
                    "MaximumListSize" => $this->getMaximumListSize(),
                    "SearchRadius" => $this->getSearchRadius(),
                ],
            ],
        ];

        $postfields = json_encode($data);

        list($http_code, $results) = $this->curlRequest(self::UPS_LIVE_URL, $postfields);

        if ($callback)
        {
            return call_user_func_array($callback, [$http_code, $results]);
        }

        return [$http_code, $results];
    }


    /**
     * Curl wrapper.
     * @param $url
     * @param $postfields
     * @return array
     * @throws Exception
     */
    private function curlRequest($url, $postfields)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);

        $result = curl_exec($curl);

        $http_code =  curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // curl_errno($curl);
        // curl_error($curl);
        if ($curl_error = curl_error($curl))
        {
            throw new Exception("API curl error: {$curl_error}");
        }

        curl_close($curl);

        return [$http_code, $result];
    }

}
