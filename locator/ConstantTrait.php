<?php namespace Ups\Locator;


use ReflectionClass;


trait ConstantTrait {

    /**
     * Returns frinedly formatted constants.
     * @return array
     */
    public static function getConstants()
    {
        $class = new ReflectionClass(__CLASS__);
        $constants = array_flip($class->getConstants());

        $all = [];

        foreach ($constants as $k => $v)
        {
            $all[$k] = self::pretifyValue($v);
        }

        return $all;
    }

    public static function pretifyValue($value)
    {
        return str_replace(['-', '_'], ' ', $value);
    }
}